# Ecofestival24 Naturalist Apps

This presentation is part of the EcoFestival 2024 on the Lyon campus of Gustave Eiffel University.

It aims to present 3 naturalist participatory science applications.

- [INPN Espèces](https://inpn.mnhn.fr/accueil/participer/inpn-especes)
- [Seek by iNaturalist](https://www.inaturalist.org/pages/seek_app)
- [Pl@ntNet](https://plantnet.org/)

## Online presentation

![QR Code](images/qr_code.png){fig-align="center"}

https://ame.gitpages.univ-eiffel.fr/ecofestival24-app-naturalistes/

# Licence



This works has been made by Nicolas Roelandt (Univ. Eiffel/AME)  and is under a [CC BY-SA 4.0 Deed](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).

![](https://licensebuttons.net/l/by-sa/3.0/88x31.png)

All screenshot were made by the author.
